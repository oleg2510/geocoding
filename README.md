# Geocoder

Это приложение является микросервисом (и соотвествено являеться частью микросервисной архетиктуры)

Дисковеринг и конфигурация сервиса осуществляется через Spring Consul (ИМХО Лучше чем Eureka).

Мониторинг чрез ELK стек (кибана и логстеш онли без эластика)

Для снятия метрик (CPU,Heap и тд) юзается Dropwizard (их можно отфорвардить на Logstash). 
По дефолту они доступны через JMX, можно подключится через jconsole

## Development

Для изи старта (в dev профиле спринга):

    docker-compose -f src/main/docker/consul.yml up -d
    docker-compose -f src/main/docker/redis.yml up -d
    docker run -p 80:8080 swaggerapi/swagger-ui

    ./gradlew

Затем открываем htpp://localhost - где у нас должен был установиться swagger ui     
  
  http://localhost:8081/v2/api-docs - Вводим в строке Explore
  
И проверяем (тестируем  - Try it out) выполненое тестовое задание прямо тут. Можно через CURL если не нравится веб морда

## Продакшн

Оптимизация под прод (добавляет компрессию Gzip, убирает отдадку уровень логов и т.п):

    ./gradlew -Pprod clean bootRepackage

Чекаем что все робит(Два варника один исполняемый включает Spring Boot, другой(original) для деплоя на внешний контейнер сервлетов):

    java -jar build/libs/*.war


Чтобы сделать образ докера (с продакшн профилем)

    ./gradlew bootRepackage -Pprod buildDocker

Потом собираем все весте:

    docker-compose -f src/main/docker/app.yml up -d
