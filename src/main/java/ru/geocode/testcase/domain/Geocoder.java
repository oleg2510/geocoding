package ru.geocode.testcase.domain;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Objects;

/**
 * Created by teligent on 20.06.17.
 */

public class Geocoder implements Serializable {

        private static final long serialVersionUID = 1L;


        private String address;

        private String location;

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getLocation() {
            return location;
        }

        public Geocoder location(String location) {
            this.location = location;
            return this;
        }

        public void setLocation(String location) {
            this.location = location;
        }


        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Geocoder geocoder = (Geocoder) o;

            if (address != null ? !address.equals(geocoder.address) : geocoder.address != null) return false;
            return location != null ? location.equals(geocoder.location) : geocoder.location == null;
        }

        @Override
        public int hashCode() {
            int result = address != null ? address.hashCode() : 0;
            result = 31 * result + (location != null ? location.hashCode() : 0);
            return result;
        }

        @Override
        public String toString() {
            return "Geocoder{" +
                "address='" + address + '\'' +
                ", location='" + location + '\'' +
                '}';
        }
}


