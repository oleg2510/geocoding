package ru.geocode.testcase.repository;

import ru.geocode.testcase.domain.Geocoder;

/**
 * Created by teligent on 20.06.17.
 */
public interface GeocoderRepository {

    void saveGeocoder(Geocoder geocoder);
    String findGeocoder(String address);
}
