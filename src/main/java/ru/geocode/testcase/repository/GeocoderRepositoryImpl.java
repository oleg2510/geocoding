package ru.geocode.testcase.repository;


import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;
import redis.clients.jedis.Jedis;
import ru.geocode.testcase.domain.Geocoder;


/**
 * Created by teligent on 20.06.17.
 */
@Repository
@Cacheable
public class GeocoderRepositoryImpl implements GeocoderRepository {

    @Override
    public void saveGeocoder(Geocoder geocoder) {
        Jedis jedis = new Jedis();
        jedis.hset("geocoder", geocoder.getAddress(),geocoder.getLocation());
    }


    @Override
    public String findGeocoder (String address) {
       Jedis jedis = new Jedis();
       String geocoder = "new_"+address;

       try {
           geocoder = jedis.hget("geocoder",address);
        } catch (Exception e){

           System.out.println("asasa "+e.getMessage());

       }


       return geocoder;
    }

}
