package ru.geocode.testcase.service;

import com.google.maps.errors.ApiException;
import ru.geocode.testcase.domain.Geocoder;
import ru.geocode.testcase.service.dto.GeocoderDTO;

import java.io.IOException;
import java.util.List;

/**
 * Service Interface for managing Geocoder.
 */
public interface GeocoderService {


    /**
     *  Get the "address" geocoder.
     *
     *  @param address the address of the entity
     *  @return the entity
     */
    GeocoderDTO findOneByAddress(String address) throws InterruptedException, ApiException, IOException;

    /**
     *  Get the "location" geocoder.
     *
     *  @param location the address of the entity
     *  @return the entity
     */
    GeocoderDTO findOneByLocation(String location) throws InterruptedException, ApiException, IOException;

}
