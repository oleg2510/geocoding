package ru.geocode.testcase.service.dto;


import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the Geocoder entity.
 */
public class GeocoderDTO implements Serializable {


    private String address;

    private String location;


    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GeocoderDTO that = (GeocoderDTO) o;

        if (address != null ? !address.equals(that.address) : that.address != null) return false;
        return location != null ? location.equals(that.location) : that.location == null;
    }

    @Override
    public int hashCode() {
        int result = address != null ? address.hashCode() : 0;
        result = 31 * result + (location != null ? location.hashCode() : 0);
        return result;
    }


    @Override
    public String toString() {
        return "GeocoderDTO{" +
            "address='" + address + '\'' +
            ", location='" + location + '\'' +
            '}';
    }
}
