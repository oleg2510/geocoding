package ru.geocode.testcase.service.impl;

import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.errors.ApiException;
import com.google.maps.model.GeocodingResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import redis.clients.jedis.Jedis;
import ru.geocode.testcase.domain.Geocoder;

import ru.geocode.testcase.repository.GeocoderRepository;
import ru.geocode.testcase.service.GeocoderService;
import ru.geocode.testcase.service.dto.GeocoderDTO;
import ru.geocode.testcase.service.mapper.GeocoderMapper;

import java.io.IOException;

/**
 * Service Implementation for managing Geocoder.
 */
@Service
@Transactional
public class GeocoderServiceImpl implements GeocoderService {

    private final Logger log = LoggerFactory.getLogger(GeocoderServiceImpl.class);

    private final GeocoderMapper geocoderMapper;

    private final GeoApiContext context = new GeoApiContext().setApiKey("AIzaSyBW07g33qFikAr132a_Kxlm60ZNsBFJoJY");

    public GeocoderServiceImpl (GeocoderMapper geocoderMapper) {
        this.geocoderMapper = geocoderMapper;
    }

    @Autowired
    GeocoderRepository repository;


    /**
     *  Get one geocoder by address.
     *
     *  @param address the address of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public GeocoderDTO findOneByAddress(String address) throws InterruptedException, ApiException, IOException {
        log.debug("Request to get Geocoder : {}", address);

        Geocoder geocoder = new Geocoder();
        String cacheLocation = repository.findGeocoder(address);

        //Сначала ищем адрес в кеше если он есть то берем его отуда если нет то делаем новый запрос
        if(cacheLocation != null && !cacheLocation.isEmpty()) {

             geocoder.setAddress(address);
             geocoder.setLocation(cacheLocation);

         } else {

            GeocodingResult[] results = GeocodingApi.geocode(context, address).await();
            geocoder.setLocation(String.valueOf(results[0].geometry.location));
            geocoder.setAddress(address);
            repository.saveGeocoder(geocoder);

         }

        return geocoderMapper.toDto(geocoder);
    }

    /**
     *  Get one geocoder by location.
     *
     *  @param location the id of the entity
     *  @return the entity
     */

    @Override
    @Transactional(readOnly = true)
    public GeocoderDTO findOneByLocation(String location) throws InterruptedException, ApiException, IOException  {
        log.debug("Request to get Geocoder : {}", location);

        Geocoder geocoder = new Geocoder();
        String cacheLocation = repository.findGeocoder(location);

        if(cacheLocation != null && !cacheLocation.isEmpty()) {

            geocoder.setLocation(location);
            geocoder.setLocation(cacheLocation);

        } else {

            GeocodingResult[] results = GeocodingApi.geocode(context, location).await();
            geocoder.setLocation(location);
            geocoder.setAddress(String.valueOf(results[0].formattedAddress));
            repository.saveGeocoder(geocoder);

        }

        return geocoderMapper.toDto(geocoder);

    }
}
