package ru.geocode.testcase.service.mapper;

import org.mapstruct.Mapper;
import ru.geocode.testcase.domain.Geocoder;
import ru.geocode.testcase.service.dto.GeocoderDTO;

/**
 * Mapper for the entity Geocoder and its DTO GeocoderDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface GeocoderMapper extends EntityMapper <GeocoderDTO, Geocoder> {


    default Geocoder fromAddress(String address) {
        if (address == null) {
            return null;
        }
        Geocoder geocoder = new Geocoder();
        geocoder.setAddress(address);
        return geocoder;
    }
}
