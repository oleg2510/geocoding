package ru.geocode.testcase.web.rest;


import com.codahale.metrics.annotation.Timed;
import com.google.maps.errors.ApiException;
import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import ru.geocode.testcase.service.GeocoderService;
import ru.geocode.testcase.service.dto.GeocoderDTO;

import java.io.IOException;
import java.util.Optional;

/**
 * Created by teligent on 20.06.17.
 * REST controller for managing Geocoder.
 */
@RestController
@RequestMapping("/api")
@Api(value="Геокодинг", tags = "геодкодинг")
public class GeocoderResource {

    private final Logger log = LoggerFactory.getLogger(GeocoderResource.class);

    @Autowired
    GeocoderService geocoderService;

    //Не совсем REST-way для сущности но через Path - санитизация спец сиволов в URL encoded (запятые и тд)

    /**
     * GET  /geocoder/address/?address= : get the "address" geocoder.
     *
     * @param address the address of the geocoderDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the geocoderDTO, or with status 404 (Not Found)
     */
    @CrossOrigin
    @Timed
    @GetMapping("/geocoder/address")
    @ApiOperation(value = "Преобразовать адрес в координаты")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Сущность с координатами") })
    public ResponseEntity<GeocoderDTO> getLocation(@RequestParam(value = "address",required = false) String address)
                    throws InterruptedException, ApiException, IOException {
        log.debug("REST request to get Geocoder : {}", address);

        GeocoderDTO geocoderDTO = geocoderService.findOneByAddress(address);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(geocoderDTO));
    }

    /**
     * GET  /geocoder/location/?location= : get the "location" geocoder.
     *
     * @param location the address of the geocoderDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the geocoderDTO, or with status 404 (Not Found)
     */
    @CrossOrigin
    @Timed
    @GetMapping("/geocoder/location")
    @ApiOperation(value = "Преобразовать координаты в адрес")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Сущность с координатами") })
    public ResponseEntity<GeocoderDTO> getAddress(@RequestParam(value = "location",required = false) String location)
        throws InterruptedException, ApiException, IOException {
        log.debug("REST request to get Geocoder : {}", location);

        GeocoderDTO geocoderDTO = geocoderService.findOneByLocation(location);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(geocoderDTO));
    }


}
