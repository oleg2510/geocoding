/**
 * View Models used by Spring MVC REST controllers.
 */
package ru.geocode.testcase.web.rest.vm;
