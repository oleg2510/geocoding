package ru.geocode.testcase.cucumber.stepdefs;

import ru.geocode.testcase.GeocodeApp;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.ResultActions;

import org.springframework.boot.test.context.SpringBootTest;

@WebAppConfiguration
@SpringBootTest
@ContextConfiguration(classes = GeocodeApp.class)
public abstract class StepDefs {

    protected ResultActions actions;

}
