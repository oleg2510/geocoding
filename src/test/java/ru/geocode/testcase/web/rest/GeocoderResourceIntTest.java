package ru.geocode.testcase.web.rest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import org.springframework.transaction.annotation.Transactional;
import ru.geocode.testcase.GeocodeApp;
import ru.geocode.testcase.domain.Geocoder;
import ru.geocode.testcase.repository.GeocoderRepository;
import ru.geocode.testcase.service.GeocoderService;


import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by teligent on 21.06.17.
 * Test class for the GeocoderResource REST controller.
 *
 * @see GeocoderResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = GeocodeApp.class)
public class GeocoderResourceIntTest {

    private static final String DEFAULT_ADDRESS = "Краснодар,Школьная 5";
    private static final String DEFAULT_LOCATION = "45.03700800,39.01822590";

    private Geocoder geocoder;

    @Autowired
    private GeocoderRepository geocoderRepository;

    @Autowired
    private MockMvc restGeocoderMockMvc;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;


    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        GeocoderResource geocoderResource = new GeocoderResource();
        this.restGeocoderMockMvc = MockMvcBuilders.standaloneSetup(geocoderResource)
            .setMessageConverters(jacksonMessageConverter).build();
    }


    public static Geocoder createEntity() {
        Geocoder geocoder = new Geocoder();
        geocoder.setAddress(DEFAULT_ADDRESS);
        geocoder.setLocation(DEFAULT_LOCATION);

        return geocoder;
    }

    @Before
    public void initTest() {
        geocoder = createEntity();
    }


    @Test
    public void getLocation() throws Exception {

        // Initialize the cache
        geocoderRepository.saveGeocoder(geocoder);

        // Get the geocoder using address
        restGeocoderMockMvc.perform(get("/api/geocoder/address/")
             .param("address",DEFAULT_ADDRESS))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));
    }

}
